# sampler

A sound sampler in c++

Uses portaudio and libsndfile libraries to play `.wav` files.

See blog post https://konradp.gitlab.io/blog/post/audio-cpp/

# Compile and test
```
cd src
make
./main
curl --unix-socket ./wavplay.sock http:/test --data @test.json
```
