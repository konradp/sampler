#include <cstring> // memset
#include <iostream>
#include <portaudio.h>
#include <sndfile.h>

#include "Player.h"
#include "SocketServer.h"
#include <json/json.h>

using std::cerr;
using std::cout;
using std::endl;
using std::ostream;

int main() {
  SocketServer server = SocketServer();
  Player player = Player();
  return 0;
}
