#include "SocketServer.h"
#include "3rdparty/tinyhttpparser/src/HttpParser.hpp"

#include <stdio.h>
#include <unistd.h>
#include <string>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>

#include <iostream>

using std::cout;

std::string sock_path = "wavplay.sock";
const char* socket_path = sock_path.c_str();

SocketServer::SocketServer() {
  struct sockaddr_un addr;
  char buf[100];
  int fd, cl, rc;

  if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    perror("socket error");
    exit(-1);
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);
  unlink(socket_path);

  if (bind(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
    perror("bind error");
    exit(-1);
  }

  if (listen(fd, 5) == -1) {
    perror("listen error");
    exit(-1);
  }

  while (1) {
    if ((cl = accept(fd, NULL, NULL)) == -1) {
      perror("accept error");
      continue;
    }

    // Init http parser
    HttpMsg* http_msg = new HttpMsg;
    HttpParser http_parser;
    while ((rc=read(cl,buf,sizeof(buf))) > 0) {
      printf("read %u bytes: %.*s\n", rc, rc, buf);
      // TODO: Do stuf fhere
      http_parser.SetBuf(buf, sizeof(buf));
      http_parser.SetHttpMsg(http_msg);
      http_parser.Parse();

      // Print out
      std::cout << "----------" << std::endl;
      std::cout << "State: " << http_parser.GetState() << std::endl;
      std::cout << "Parsed" << std::endl;
      std::cout << "Front: " << http_msg->GetFront() << std::endl;
      std::cout << "Header: " << std::endl << http_msg->GetHeader().GetStr();
      std::cout << "Header done." << std::endl;
      std::cout << "Body: " << http_msg->GetBody() << std::endl;
      std::cout << "----------" << std::endl;
    }
    if (rc == -1) {
      perror("read");
      exit(-1);
    }
    else if (rc == 0) {
      printf("EOF\n");
      close(cl);
    }
  }
  //return 0;
}
