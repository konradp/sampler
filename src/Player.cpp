#include <cstring> // memset
#include <iostream>
#include <portaudio.h>
#include <sndfile.h>

#include "Player.h"
#include "SocketServer.h"

using std::cerr;
using std::cout;
using std::endl;
using std::ostream;

ostream& operator << (ostream& os, const PaDeviceInfo& i) {
  return os << "defaultSampleRate: " << i.defaultSampleRate << endl;
}

typedef struct {
    SNDFILE     *file;
    SF_INFO      info;
} callback_data_s;

static int
paCallback(
    const void *input,
    void *output,
    unsigned long frameCount,
    const PaStreamCallbackTimeInfo *timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData
) {
    float *out = (float*)output;;
    callback_data_s *p_data = (callback_data_s*)userData;
    sf_count_t num_read;
    // Clear output buffer
    memset(
      out,
      0,
      sizeof(float) * frameCount * p_data->info.channels
    );
    // Read into output buffer
    num_read = sf_read_float(
      p_data->file,
      out,
      frameCount * p_data->info.channels
    );
    if(num_read < frameCount) {
      // Can't read a full frameCount of samples, then reached EOF
      return paComplete;
    }
    return paContinue;
}


Player::Player() {
  callback_data_s data;
  //sfinfo.format = 0;
  PaStream* stream;
  PaStream* adsr;
  PaError err;

  data.file = sf_open("../sine.wav", SFM_READ, &data.info);
  if(sf_error(data.file) != SF_ERR_NO_ERROR) {
    cerr << "Unable to open file: "
      << sf_strerror(data.file) << endl;
    //return 1;
  }

  // DEBUG
  cout << "INFO: " << endl
    << "f: " << data.info.frames << endl
    << "sr: " << data.info.samplerate << endl
    ;

  // Initialise portaudio
  err = Pa_Initialize();
  if(err != paNoError) {
    cerr << "ERROR: Unable to init PortAudio" << endl
         << Pa_GetErrorText(err) << endl;
    //return 1;
  } else {
    cout << "wavplay: Initialised portaudio" << endl;
  }
  // Open stream
  err = Pa_OpenDefaultStream(
    &stream,            // stream
    0,                  // in channels: none
    data.info.channels, // out channels
    paFloat32,          // sample format
    data.info.samplerate,
    //data.info.frames,   // framesPerBuffer
    data.info.samplerate/10,   // framesPerBuffer
    paCallback,
    &data);
  if(err != paNoError) {
    cerr << "Unable to open default stream" << endl;
    //return 1;
  }
  // Start stream
  err = Pa_StartStream(stream);
  if(err != paNoError) {
    cerr << "Unable to start stream" << endl;
    //return 1;
  }
  while(Pa_IsStreamActive(stream)) {
    Pa_Sleep(100);
  }
  // Close stream
  sf_close(data.file);
  Pa_Terminate();
  //return 0;
}
